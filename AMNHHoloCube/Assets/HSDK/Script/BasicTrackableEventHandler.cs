﻿using UnityEngine;
using Vuforia;

public class BasicTrackableEventHandler : MonoBehaviour, ITrackableEventHandler
{

	private TrackableBehaviour mTrackableBehaviour;

	public delegate void TrackingEvent();
	public event TrackingEvent OnTrackingFound;
	public event TrackingEvent OnTrackingLost;

	[Tooltip("These are the objects that should disable when the tracker is lost and enable when the tracker is found.")]
	public GameObject[] objectsToHide;

	public bool isTracking { get; private set; }

	void Start()
	{
		isTracking = false;

		mTrackableBehaviour = GetComponent<TrackableBehaviour>();

		if (mTrackableBehaviour)
		{
			mTrackableBehaviour.RegisterTrackableEventHandler(this);
		}

		OnTrackingFound += HandleTrackingFound;
		OnTrackingLost += HandleTrackingLost;
	}

	public void OnTrackableStateChanged( TrackableBehaviour.Status previousStatus, TrackableBehaviour.Status newStatus )
	{
		if (newStatus == TrackableBehaviour.Status.DETECTED ||
			newStatus == TrackableBehaviour.Status.TRACKED ||
			newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
		{
			isTracking = true;

			if (OnTrackingFound != null)
			{
				OnTrackingFound();
			}
		}
		else
		{
			isTracking = false;

			if (OnTrackingLost != null)
			{
				OnTrackingLost();
			}

			if (this.GetComponent<InputExampleManager>().FossilModel [0] != null) {
				SendMessage ("ResetModel", SendMessageOptions.DontRequireReceiver);
			}

		}
	}



	void HandleTrackingFound()
	{
		if (objectsToHide != null)
		{
			for (int index = 0; index < objectsToHide.Length; index++)
			{
				objectsToHide[index].SetActive(true);
			}
		}

		TrackingFound ();
	}

	void HandleTrackingLost()
	{
		if (objectsToHide != null)
		{
			for (int index = 0; index < objectsToHide.Length; index++)
			{
				if (objectsToHide[index] != null)
					objectsToHide[index].SetActive(false);
			}
		}

		TrackingLost ();
	}

	#region PRIVATE_METHODS


	private void TrackingFound()
	{
		Renderer[] rendererComponents = GetComponentsInChildren<Renderer>(true);
		Collider[] colliderComponents = GetComponentsInChildren<Collider>(true);

		// Enable rendering:
		foreach (Renderer component in rendererComponents)
		{
			component.enabled = true;
		}

		// Enable colliders:
		foreach (Collider component in colliderComponents)
		{
			component.enabled = true;
		}

		Debug.Log("Trackable " + mTrackableBehaviour.TrackableName + " found");
	}


	private void TrackingLost()
	{
		Renderer[] rendererComponents = GetComponentsInChildren<Renderer>(true);
		Collider[] colliderComponents = GetComponentsInChildren<Collider>(true);

		// Disable rendering:
		foreach (Renderer component in rendererComponents)
		{
			component.enabled = false;
		}

		// Disable colliders:
		foreach (Collider component in colliderComponents)
		{
			component.enabled = false;
		}

		Debug.Log("Trackable " + mTrackableBehaviour.TrackableName + " lost");
	}

	#endregion // PRIVATE_METHODS

}