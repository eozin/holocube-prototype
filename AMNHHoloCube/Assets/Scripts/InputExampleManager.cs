﻿/*==============================================================================
      Edited by Eozin Che, Fall 2017, Science Visualization Group, AMNH     
==============================================================================*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputExampleManager : MonoBehaviour {
	public Transform cubeRoot;
	public Renderer [] cubeSides;
	public GameObject[] FossilModel;

	int cubeClickCount = 1;
	public int fossilClickCount = 0;

	public Color [] colorPool;
	int colorIndex = 0;

	public bool alwaysReMatch = false;
	public bool alreadyMatch = false;

	void Start(){
		GetComponent<BasicTrackableEventHandler>().OnTrackingFound += OnTrackingFound;
		GetComponent<BasicTrackableEventHandler>().OnTrackingLost += OnTrackingLost;
	}

	void OnTrackingFound(){
		if (!alreadyMatch) {
			transform.FaceToCamera (cubeRoot);
			alreadyMatch = !alwaysReMatch;
		}
	}
	void OnTrackingLost(){
		
	}
		
	//When the HoloCube is clicked
	public void CubeClick(){
		cubeClickCount++;
		if (cubeClickCount > 5) {
			cubeClickCount = 1;
		}
		Vector2 sizeTp = Vector2.one * (float)cubeClickCount;
		foreach (Renderer tp in cubeSides) {
			tp.material.SetTextureScale ("_decal", sizeTp);
		}
	}

	public void NextClick(){
		colorIndex++;
		if (colorIndex >= colorPool.Length) {
			colorIndex = 0;
		}
		foreach (Renderer tp in cubeSides) {
			tp.material.SetColor("_basecolor", colorPool[colorIndex]);
		}
	}
		

	//when the fossil is clicked
	public void FossilClick(){
		fossilClickCount++;

		if (fossilClickCount > 2) {
			fossilClickCount = 1;
		}

		if (fossilClickCount == 1) {
			if (FossilModel [0] != null) {
				FossilModel [0].transform.GetChild (0).gameObject.SetActive (true);
				FossilModel [0].transform.GetChild (1).gameObject.SetActive (true);
				FossilModel [0].transform.GetChild (2).gameObject.SetActive (false);    
				FossilModel [0].GetComponent<Animator> ().Play ("Split", -1, 0f);

				foreach (Animator animController in FossilModel [0].GetComponentsInChildren<Animator>()) {
					animController.Play ("ScaleUpAnim", -1, 0f);
				}
					
			}
		}

		if (fossilClickCount == 2) {
			if (FossilModel [0] != null) {
				FossilModel [0].GetComponent<Animator> ().Play ("Combine", -1, 0f);
				foreach (Animator animController in FossilModel [0].GetComponentsInChildren<Animator>()) {
					animController.Play ("ScaleDownAnim", -1, 0f);
				}
				StartCoroutine (SwitchFossilPart (2.4f));
			}
		}

	}

	IEnumerator SwitchFossilPart(float waitTime){
		yield return new WaitForSeconds(waitTime);
		FossilModel [0].transform.GetChild (0).gameObject.SetActive (false);
		FossilModel [0].transform.GetChild (1).gameObject.SetActive (false);
		FossilModel [0].transform.GetChild (2).gameObject.SetActive (true);    
	}

	public void ResetModel(){
		FossilModel [0].transform.GetChild (0).gameObject.SetActive (false);
		FossilModel [0].transform.GetChild (1).gameObject.SetActive (false);
		FossilModel [0].transform.GetChild (2).gameObject.SetActive (true);    
		fossilClickCount = 0;
	}

}
