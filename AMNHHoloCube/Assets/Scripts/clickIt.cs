﻿/*==============================================================================
      Written by Eozin Che, Fall 2017, Science Visualization Group, AMNH     
==============================================================================*/
 
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class clickIt : MonoBehaviour {

	void Start () {
		
	}

	void Update () {
		if (Input.GetMouseButtonDown(0)){
			RaycastHit hitInfo;
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			if (Physics.Raycast (ray, out hitInfo, 100)) {
				hitInfo.transform.SendMessage ("OnClick", SendMessageOptions.DontRequireReceiver);
			}
		}
	}


}
